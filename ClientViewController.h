//
//  ClientViewController.h
//  ControleDeConta
//
//  Created by Bob Wei on 14-8-18.
//  Copyright (c) 2014年 Bob Wei. All rights reserved.
//

#import "ViewController.h"

@interface ClientViewController : ViewController
- (void)loadRequestFromString:(NSString*)urlString;
@property NSMutableData *webData;
@property NSXMLParser *xmlParser;
@property NSString *finalData;
@property NSString *convertToStringData;
@property NSMutableString *nodeContent;
@property (weak, nonatomic) IBOutlet UIButton *invokeService;

@end
