//
//  ClientViewController.m
//  ControleDeConta
//
//  Created by Bob Wei on 14-8-18.
//  Copyright (c) 2014年 Bob Wei. All rights reserved.
//

#import "ClientViewController.h"

@interface ClientViewController ()<NSXMLParserDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *site;
@end

@implementation ClientViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *fullURL = @"http://www.culturachinesa.com.br/eagle/";
    NSURL *url = [NSURL URLWithString:fullURL];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [ _site loadRequest:requestObj];
    
    //NSBundle *mainBundle = [NSBundle mainBundle];
    //NSString *myFile = [mainBundle pathForResource: @"login" ofType: @"html"];
 //1   NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"login" ofType:@"html"];
//    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
//2    [self.site loadHTMLString:htmlString baseURL:[[NSBundle mainBundle] bundleURL]];
    //[self loadRequestFromString:myFile];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadRequestFromString:(NSString *)urlString{
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [self.site loadRequest:urlRequest];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)invokeService:(id)sender {
    
}

@end
